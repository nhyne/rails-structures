class LinkedList
  def initialize(data)
    @head = ListNode.new(data)
    @tail = @head
  end

  def insert(data)
    node = ListNode.new(data)
    node.previous_node=(@tail)
    @tail.next_node=(node)
    @tail = node
  end

  def delete_index(pos)

  end

  def delete_head
    @head = @head.next_node
  end

  def delete_tail
    @tail = @tail.previous_node
  end

  def search(value)
    val = @head
    returner = 0
    while val != nil do
      return returner if val.data == value
      returner += 1
      val = val.next_node
    end
    return nil
  end

  def [](key)

  end

  def reverse

  end

  def length

  end

  def insert_at(data, pos)

  end

  def print
    val = @head
    while val != nil do
      puts val.data
      val = val.next_node
    end
  end

end




class ListNode
  def initialize(data)
    @data = data || nil
    @next = nil
    @previous = nil
  end

  def next_node
    @next
  end

  def previous_node
    @previous
  end

  def data
    @data
  end

  def next_node=(listnode)
    @next = listnode
  end

  def previous_node=(listnode)
    @previous = listnode
  end
end


a = LinkedList.new('adam')
a.insert(' johnson')

puts a.search('johnson')
a[1]
